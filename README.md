##Instalar Twig:

Primero necesitas un archivo llamado composer.json con el siguiente codigo:
`{
   "require": {
       "twig/twig": "1.*"
   }
 }`

,y luego debe ejecutarse en la terminal el comando `curl -s http://getcomposer.org/installer |php`.

Y otro llamado composer.phar con el código de "https://getcomposer.org/installer" y luego hay que ejecutar el comando `php composer.phar install` en un terminal.

##Funcionalidad:

El proyecto funciona con una clase principal llamada **index.php** en la base del proyecto dividido en 6 apartados, uno para mostrar el cuerpo principal de la aplicación, otra para insertar nuevas recetas, otra para borrarlas, y otra para editarlas cada parte con sus respectivos templates.

Los templates usan una plantilla principal llamada **plantilla_principal.html**. La parte de insertar tiene 3 templates uno para insertar las recetas, otro para insertar los ingredientes y un último para insertar los pasos de la elaboración de la receta; la parte de editar también usa tres templates de la misma forma para las recetas, ingredientes y pasos de elaboración y la de borrar no tiene templates se ejecuta directamente desde el cuerpo principal. 

La aplicación usa 4 archivos para registrarse **login.php** que contiene el form para registrarse una vez ejecutado conecta directamente con **comprobar.php** este  archivo comprueba que los datos que has metido en el formulario son correctos conectando con la base de datos y con **PasswordHash.php** un archivo con funciones capaz de traducir las contraseñas de django de la base de datos, si todo va bien te redirige a la aplicación si va mal te redirige de nuevo a **login.php**, también tiene un archivo llamado **logout.php** para salir de la sesión.

La aplicación hace uso de de fuentes que se almacenan en la carpeta fuentes, las imágenes predefinidas se guardan en las carpeta imagenes y las que se suben los usuarios en la carpeta subir que también se borrar automáticamente según la aplicación.

##Uso:

La aplicación nos permite ver las recetas y si nos registramos también nos permitirá:

- Crear nuestra propias recetas.
- Modificar recetas pero solo las nuestras.
- Borrar recetas pero solo las nuestras.