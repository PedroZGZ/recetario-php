<?php
session_start();
if(isset($_SESSION['usr']) && isset($_SESSION['pswd'])){
	header("Location: index.php");
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
	<head>
		<title> PHP Login </title>
		
		<link rel="shortcut icon" href="imagenes/favicon.png" type="image/png" />
		<link rel="stylesheet" type="text/css" href="estilos_log.css">
		<meta charset="utf-8" />
	<title>Image-less CSS3 Glowing Form Implementation</title>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
	<script>
		$(function(){
			var $form_inputs =   $('form input');
			var $rainbow_and_border = $('.rain, .border');
			/* Used to provide loping animations in fallback mode */
			$form_inputs.bind('focus', function(){
				$rainbow_and_border.addClass('end').removeClass('unfocus start');
			});
			$form_inputs.bind('blur', function(){
				$rainbow_and_border.addClass('unfocus start').removeClass('end');
			});
			$form_inputs.first().delay(800).queue(function() {
				$(this).focus();
			});
		});
	</script>
	</head>
	<body id="home">
		<div class="rain">
		<div class="border start">
			<form method="post" action="comprobar.php">
				<label>Usuario</label><input name="usr" type="text" placeholder="Usuario"/>
				<label>Contraseña</label><input name="pswd" type="password" placeholder="Contraseña"/>
				<input type="submit" value="Entrar"/>
			</form>
		</div>
		</div>
	</body>
</html>
