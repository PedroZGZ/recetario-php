<?php
session_start();
require_once 'vendor/twig/twig/lib/Twig/Autoloader.php';
Twig_Autoloader::register();

try {
  $db = new PDO('sqlite:db.sqlite3');
} catch (PDOException $e) {
  echo "Error: Could not connect. " . $e->getMessage();
}

$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

if(isset($_GET['fase']) ? $_GET['fase']: ""){
	$fase = $_GET['fase'];
}elseif (isset($_POST['fase']) ? $_POST['fase']: ""){
	$fase = $_POST['fase'];
}else{
	$fase = "principal";
}

$falloDB = "<p id='fallo'><strong>No has metido bien los datos:</strong> faltan datos en el registro o no son invalidos.</p>";
$falloDBrepetido = "<p id='fallo'><strong>No has metido bien los datos:</strong> no puedes meter datos repetidos.</p>";

try {
  // ---------------------------------------------------------- Neutro ----------------------------------------------------------
  
  // ---------------------------------------------------------- Principal ----------------------------------------------------------
  if($fase=="principal"){
	$sql = "SELECT DISTINCT * FROM misrecetas_pasos_elaboracion";
	$resultados = $db->query($sql);
	while ($row = $resultados->fetchObject()) {
		$consultaPasosElaboracion[] = $row;
	} 

	$sql = "SELECT * FROM misrecetas_receta ORDER BY nombre";
	$resultados = $db->query($sql);
	while ($row = $resultados->fetchObject()) {
		$consultaRecetas[] = $row;
	}
  
	$sql = "SELECT DISTINCT * FROM misrecetas_ingredientes";
	$resultados = $db->query($sql);
	while ($row = $resultados->fetchObject()) {
		$consultaIngredientes[] = $row;
	}
  }
  // ---------------------------------------------------------- Principal ----------------------------------------------------------

  // ---------------------------------------------------------- Insertar ----------------------------------------------------------
  if($fase=="insertar"){
  
	$nombre = isset($_POST['nombre']);
	if ($nombre){
		$nombre = $_POST['nombre'];
		$tipo = $_POST['tipo'];
		$tiempo_elaboracion = $_POST['tiempo_elaboracion'];
		$fecha_creacion = $_POST['fecha_creacion'];
		$fecha_modificacion = $_POST['fecha_modificacion'];
		$editor = $_POST['editor'];
	

		$name = $_FILES['imagen']['name'];
		$tmp_name = $_FILES['imagen']['tmp_name'];
		$error = $_FILES['imagen']['error'];
		$size = $_FILES['imagen']['size'];
		$max_size = 1024 * 1024 *1;
		$type = $_FILES['imagen']['type'];

		if ($error){
		}else if ($size > $max_size){
		}else if($type != "image/jpeg" && $type != "image/png" && $type != "image/gif"){
		}else{
			$ruta = "subir/".$name;
			move_uploaded_file($tmp_name, $ruta);
		}

		try{
			$insertar = "INSERT INTO misrecetas_receta (nombre, tipo, tiempo_elaboracion, fecha_creacion, fecha_modificacion, imagen, editor)
				VALUES(:nombre, :tipo, :tiempo_elaboracion, :fecha_creacion, :fecha_modificacion, :name, :editor)";
			$sentencia = $db->prepare($insertar);
			
			
			$sentencia->execute(array(":nombre" => $nombre, ":tipo" => $tipo, ":tiempo_elaboracion" => $tiempo_elaboracion,
			":fecha_creacion" => $fecha_creacion, ":fecha_modificacion" => $fecha_modificacion, ":name" => $name,
			":editor" => $editor));
		}catch(Exception $e){
			echo $falloDBrepetido;
		}
	}
  
	$ingrediente = isset($_POST['ingrediente']);
	if ($ingrediente){
		$ingrediente = $_POST['ingrediente'];

		if ($ingrediente != "vacio"){
			$nombre = $_POST['buscar'];
	
			$consulta = "SELECT * FROM misrecetas_receta WHERE nombre = :nombre";
			$resultados = $db->prepare($consulta);
			$resultados->execute(array(":nombre" => $nombre));
			foreach($resultados as $row) {
				extract($row);
				try{
					$insertar = "INSERT INTO misrecetas_ingredientes (id_receta_id, ingrediente) VALUES(:id, :ingrediente)";
					$sentencia = $db->prepare($insertar);
					$sentencia->execute(array(":id" => $id, ":ingrediente" => $ingrediente));
				}catch(Exception $e){
					echo $falloDB;
				}
			}
		}
	}
  
	$pasoElaboracion = isset($_POST['pasoElaboracion']);
	if ($pasoElaboracion){
		$pasoElaboracion = $_POST['pasoElaboracion'];

		if ($pasoElaboracion != "vacio"){
			$nombre = $_POST['buscar'];
	
			$consulta = "SELECT * FROM misrecetas_receta WHERE nombre = :nombre";
			$resultados = $db->prepare($consulta);
			$resultados->execute(array(":nombre" => $nombre));
			foreach($resultados as $row) {
				extract($row);
				try{
					$insertar = "INSERT INTO misrecetas_pasos_elaboracion (id_receta_id, paso_elaboracion) VALUES(:id, :pasoElaboracion)";
					$sentencia = $db->prepare($insertar);
					$sentencia->execute(array(":id" => $id, ":pasoElaboracion" => $pasoElaboracion));
				}catch(Exception $e){
					echo $falloDB;
				}
			}
		}
	}
  

	if ($nombre){
		$consulta = "SELECT * FROM misrecetas_receta WHERE nombre = :nombre";
		$resultados = $db->prepare($consulta);
		$resultados->execute(array(":nombre" => $nombre));
		foreach($resultados as $row) {
			extract($row);
			$sql = "SELECT DISTINCT * FROM misrecetas_pasos_elaboracion WHERE id_receta_id = :id";
			$resultados = $db->prepare($sql);
			$resultados->execute(array(":id" => $id));
			while ($row = $resultados->fetchObject()) {
				$consultaPasosElaboracion[] = $row;
			}
		} 
  
		$consulta = "SELECT * FROM misrecetas_receta WHERE nombre = :nombre";
		$resultados = $db->prepare($consulta);
		$resultados->execute(array(":nombre" => $nombre));
		foreach($resultados as $row) {
			extract($row);
			$sql = "SELECT DISTINCT * FROM misrecetas_ingredientes WHERE id_receta_id = :id";
			$resultados = $db->prepare($sql);
			$resultados->execute(array(":id" => $id));
			while ($row = $resultados->fetchObject()) {
				$consultaIngredientes[] = $row;
			}
		} 
	}else{
		$consultaIngredientes="No hay nada";
		$consultaPasosElaboracion="No hay nada";
	}
  }
  
  // ---------------------------------------------------------- Insertar ----------------------------------------------------------
  
  // ---------------------------------------------------------- Borrar ----------------------------------------------------------
  
  if($fase=="borrar"){
	
	if(isset($_GET['imagen']) ? $_GET['imagen']: ""){
		$imagen = $_GET['imagen'];
		unlink('subir/'.$imagen);
	}
	$id = $_GET['id'];
	
	$borrarRecetas = "DELETE FROM misrecetas_receta WHERE id=:id";
	$result = $db->prepare($borrarRecetas);
	$result->execute(array(":id" => $id));
	
	$borrarIngredientes = "DELETE FROM misrecetas_ingredientes WHERE id_receta_id=:id";
	$result = $db->prepare($borrarIngredientes);
	$result->execute(array(":id" => $id));
	
	$borrarPasosElaboracion = "DELETE FROM misrecetas_pasos_elaboracion WHERE id_receta_id=:id";
	$result = $db->prepare($borrarPasosElaboracion);
    $result->execute(array(":id" => $id));
	
	$sql = "SELECT DISTINCT * FROM misrecetas_ingredientes";
	$resultados = $db->query($sql);
	while ($row = $resultados->fetchObject()) {
		$consultaIngredientes[] = $row;
	}

	$sql = "SELECT DISTINCT * FROM misrecetas_pasos_elaboracion";
	$resultados = $db->query($sql);
	while ($row = $resultados->fetchObject()) {
		$consultaPasosElaboracion[] = $row;
	}

	$sql = "SELECT * FROM misrecetas_receta ORDER BY nombre";
	$resultados = $db->query($sql);
	while ($row = $resultados->fetchObject()) {
		$consultaRecetas[] = $row;
	}
  }
  
  // ---------------------------------------------------------- Borrar ----------------------------------------------------------
  
  // ---------------------------------------------------------- Editar ----------------------------------------------------------
  
  if($fase=="editar"){
  
	$accionEditar2 = isset($_GET['accionEditar2']) ? $_GET['accionEditar2']: "";
	$accionEditar3 = isset($_POST['accionEditar3']) ? $_POST['accionEditar3']: "";
	if($accionEditar3=="Modificar"){
		$id = $_POST['id'];
		$nombre = $_POST['nombre'];
		$tipo = $_POST['tipo'];
		$tiempo_elaboracion = $_POST['tiempo_elaboracion'];
		$fecha_modificacion = $_POST['fecha_modificacion'];
		$editor = $_POST['editor'];
	
		if (isset($_FILES['imagen']) ? $_FILES['imagen']: ""){

			$name = $_FILES['imagen']['name'];
			$tmp_name = $_FILES['imagen']['tmp_name'];
			$error = $_FILES['imagen']['error'];
			$size = $_FILES['imagen']['size'];
			$max_size = 1024 * 1024 *1;
			$type = $_FILES['imagen']['type'];

		if ($size > $max_size){
		}else if($type != "image/jpeg" && $type != "image/png" && $type != "image/gif"){
		}else{
			$ruta = "subir/".$name;
			move_uploaded_file($tmp_name, $ruta);
			
			if (isset($_POST['laImagen']) ? $_POST['laImagen']: ""){
			$laImagen = $_POST['laImagen'];
			unlink('subir/'.$laImagen);
			}
			
			$editar = "UPDATE misrecetas_receta SET imagen=:name WHERE id=:id";
			$result = $db->prepare($editar);
			$result->execute(array(":name" => $name, ":id" => $id));
		}	
	}
	try{
		$editar = "UPDATE misrecetas_receta SET id=:id, nombre=:nombre, tipo=:tipo, tiempo_elaboracion=:tiempo_elaboracion,
		fecha_modificacion=:fecha_modificacion, editor=:editor WHERE id=:id";
		$result = $db->prepare($editar);
		$result->execute(array(":id" => $id, ":nombre" => $nombre, ":tipo" => $tipo, ":tiempo_elaboracion" => $tiempo_elaboracion,
		":fecha_modificacion" => $fecha_modificacion, ":editor" => $editor));
	}catch(Exception $e){
		echo $falloDBrepetido;
	}
  }
  $accionEditar4 = isset($_POST['accionEditar4']) ? $_POST['accionEditar4']: "";
  $entrar = isset($_POST['final']) ? $_POST['final']: "";
  if(($accionEditar4=="Modificar") && ($entrar)){
	$total = $_POST["final"];
	for ($i=1 ; $i<=$total ; $i++) {
		if (isset($_POST['id'.$i])){
			$id = $_POST['id'.$i];
			try{
				$editar = "UPDATE misrecetas_ingredientes SET ingrediente=:id WHERE id=:i";
				$result = $db->prepare($editar);
				$result->execute(array(":id" => $id, ":i" => $i));
			}catch(Exception $e){
				echo $falloDBrepetido;
			}
		}
	}
  }

  $accionEditar4Insertar = isset($_POST['accionEditar4Insertar']) ? $_POST['accionEditar4Insertar']: "";
  if($accionEditar4Insertar=="Insertar"){
	$id = $_POST['id'];
	$ingrediente = $_POST['ingrediente'];
	try{
		$insertar = "INSERT INTO misrecetas_ingredientes (id_receta_id, ingrediente) VALUES(:id, :ingrediente)";
		$sentencia = $db->prepare($insertar);
		$sentencia->execute(array(":id" => $id, ":ingrediente" => $ingrediente));
	}catch(Exception $e){
		echo $falloDB;
	}
  }
  
  $accionEditar5 = isset($_POST['accionEditar5']) ? $_POST['accionEditar5']: "";
  if($accionEditar5=="Modificar"){
	$total = $_POST["final"];
	for ($i=1 ; $i<=$total ; $i++) {
		if (isset($_POST['id'.$i])){
			$id = $_POST['id'.$i];
			try{
				$editar = "UPDATE misrecetas_pasos_elaboracion SET paso_elaboracion=:id WHERE id=:i";
				$result = $db->prepare($editar);
				$result->execute(array(":id" => $id, ":i" => $i));
			}catch(Exception $e){
				echo $falloDB;
			}
		}
	}
  }

  $accionEditar5Insertar = isset($_POST['accionEditar5Insertar']) ? $_POST['accionEditar5Insertar']: "";
  if($accionEditar5Insertar=="Insertar"){
	$id = $_POST['id'];
	$pasoElaboracion = $_POST['pasoElaboracion'];
	try{
		$insertar = "INSERT INTO misrecetas_pasos_elaboracion (id_receta_id, paso_elaboracion) VALUES(:id, :pasoElaboracion)";
		$sentencia = $db->prepare($insertar);
		$sentencia->execute(array(":id" => $id, ":pasoElaboracion" => $pasoElaboracion));
	}catch(Exception $e){
		echo $falloDB;
	}
  }

  if(($accionEditar4=="Modificar") || ($accionEditar5Insertar=="Insertar")){
		$id = $_POST['id'];
		
		$sql = "SELECT * FROM misrecetas_pasos_elaboracion WHERE id_receta_id = :id";
		$resultados = $db->prepare($sql);
		$resultados->execute(array(":id" => $id));
		while ($row = $resultados->fetchObject()) {
			$consultaPasosElaboracion[] = $row;
		
		}

	}else{
		$sql = "SELECT DISTINCT * FROM misrecetas_pasos_elaboracion";
		$resultados = $db->query($sql);
		while ($row = $resultados->fetchObject()) {
			$consultaPasosElaboracion[] = $row;
		}
	}
	
	

	if(($accionEditar3=="Modificar") || ($accionEditar4Insertar=="Insertar")){
		$id = $_POST['id'];
		
		$sql = "SELECT * FROM misrecetas_ingredientes WHERE id_receta_id = :id";
		$resultados = $db->prepare($sql);
		$resultados->execute(array(":id" => $id));
		while ($row = $resultados->fetchObject()) {
			$consultaIngredientes[] = $row;
		}
	}else{
		$sql = "SELECT DISTINCT * FROM misrecetas_ingredientes";
		$resultados = $db->query($sql);
		while ($row = $resultados->fetchObject()) {
			$consultaIngredientes[] = $row;
		}
	}
  
	if(($accionEditar2=="editar") || ($accionEditar4Insertar=="Insertar")){
		if ($accionEditar2=="editar"){
			$id = $_GET['id'];
		}else{
			$id = $_POST['id'];
		}

		$sql = "SELECT * FROM misrecetas_receta WHERE id = :id";
		$resultados = $db->prepare($sql);
		$resultados->execute(array(":id" => $id));
		while ($row = $resultados->fetchObject()) {
			$consultaRecetas[] = $row;
		}
	}else{
		$sql = "SELECT * FROM misrecetas_receta ORDER BY nombre";
		$resultados = $db->query($sql);
		while ($row = $resultados->fetchObject()) {
			$consultaRecetas[] = $row;
		}
	}
  }
  
  // ---------------------------------------------------------- Editar ----------------------------------------------------------
  
  // ---------------------------------------------------------- Neutro ----------------------------------------------------------
  
  unset($db); 

  $loader = new Twig_Loader_Filesystem('templates/');
  
  $twig = new Twig_Environment($loader);
  
  // ---------------------------------------------------------- Neutro ----------------------------------------------------------
  
  // ---------------------------------------------------------- Principal ----------------------------------------------------------
 
  if($fase=="principal"){
  
	$template = $twig->loadTemplate('ver.html');
  
	if(isset($_SESSION['usr'])){
		echo $template->render(array(
			'titulo' => 'P�gina principal',
			'consultaPasosElaboracion' => $consultaPasosElaboracion,
			'consultaIngredientes' => $consultaIngredientes,
			'consultaRecetas' => $consultaRecetas,
			'usuario' => $_SESSION['usr'],
		));
	}else{
		echo $template->render(array(
			'titulo' => 'P�gina principal',
			'consultaPasosElaboracion' => $consultaPasosElaboracion,
			'consultaIngredientes' => $consultaIngredientes,
			'consultaRecetas' => $consultaRecetas,
		));
	}
  }
  // ---------------------------------------------------------- Principal ----------------------------------------------------------
  
  // ---------------------------------------------------------- Insertar ----------------------------------------------------------

  if($fase=="insertar"){
	$template = $twig->loadTemplate('insertar1.html');
  
	if ($nombre){
		$template = $twig->loadTemplate('insertar2.html');
	}
	$insertarElaboracion = isset($_POST['insertarElaboracion']);
	if ($insertarElaboracion){
		$template = $twig->loadTemplate('insertar3.html');
		$pasar = isset($_POST['pasar']);
		if ($pasar){
			$nombre = $_POST['pasar'];
		}
	}
	$crearIngrediente = isset($consultaIngredientes);
	if ($crearIngrediente){
		$crearPasoElaboracion = isset($consultaPasosElaboracion);
		if ($crearPasoElaboracion){
			echo $template->render(array(
				'usuario' => $_SESSION['usr'],
				'fecha' => date("d-m-Y"),
				'titulo' => 'Insertar receta',
				'nombre' => $nombre,
				'consultaPasosElaboracion' => $consultaPasosElaboracion,
				'consultaIngredientes' => $consultaIngredientes,
			));
		}else{
			echo $template->render(array(
				'usuario' => $_SESSION['usr'],
				'titulo' => 'Insertar ingredientes',
				'nombre' => $nombre,
				'consultaIngredientes' => $consultaIngredientes,
			));
		}
	}else{
		echo $template->render(array(
			'usuario' => $_SESSION['usr'],
			'fecha' => date("d-m-Y"),
			'titulo' => 'Insertar ingredientes',
			'nombre' => $nombre,
		));
	}
  }
  // ---------------------------------------------------------- Insertar ----------------------------------------------------------
  
  // ---------------------------------------------------------- Borrar ----------------------------------------------------------
  
  if($fase=="borrar"){
	$template = $twig->loadTemplate('ver.html');

	echo $template->render(array(
		'usuario' => $_SESSION['usr'],
		'titulo' => 'P�gina principal',
		'consultaPasosElaboracion' => $consultaPasosElaboracion,
		'consultaIngredientes' => $consultaIngredientes,
		'consultaRecetas' => $consultaRecetas,
	));
  }
  
  // ---------------------------------------------------------- Borrar ----------------------------------------------------------
  
  // ---------------------------------------------------------- Editar ----------------------------------------------------------
  
  if($fase=="editar"){

	$template = $twig->loadTemplate('ver.html');
  
	if($accionEditar2=="editar"){
		$template = $twig->loadTemplate('editar1.html');
	}
  
	if(($accionEditar3=="Modificar") || ($accionEditar4Insertar=="Insertar")){
		$template = $twig->loadTemplate('editar2.html');
	}
  
	if(($accionEditar4=="Modificar") || ($accionEditar5Insertar=="Insertar")){
		$template = $twig->loadTemplate('editar3.html');
	}
  
	$losIngredientes = isset($consultaIngredientes);
	$laElaboracion = isset($consultaPasosElaboracion);
	if (($losIngredientes) && ($laElaboracion)){
		echo $template->render(array(
			'usuario' => $_SESSION['usr'],
			'fecha' => date("d-m-Y"),
			'titulo' => 'Editar receta',
			'consultaPasosElaboracion' => $consultaPasosElaboracion,
			'consultaIngredientes' => $consultaIngredientes,
			'consultaRecetas' => $consultaRecetas,
			'id' => $id,
		));
	}else{
		echo $template->render(array(
			'usuario' => $_SESSION['usr'],
			'titulo' => 'P�gina principal',
			'fecha' => date("d-m-Y"),
			'id' => $id,
		));
	}
  }
  
  // ---------------------------------------------------------- Editar ----------------------------------------------------------

} catch (Exception $e) {
  die ('ERROR: ' . $e->getMessage());
}
?>